package net7.xyz.android_home_13_receiver_notification.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import net7.xyz.android_home_13_receiver_notification.MainActivity;
import net7.xyz.android_home_13_receiver_notification.R;

import static net7.xyz.android_home_13_receiver_notification.MainActivity.BLUETOOTH;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSOFF;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSON;
import static net7.xyz.android_home_13_receiver_notification.utils.NotifyUtils.sendMyBroadcast;

/**
 * Created by user on 18.02.2018.
 */

public class BluetoothReceiver extends BroadcastReceiver {
    public final int REQ_CODE = 107;
    public final int NOTIFY_ID = 108;

    public BluetoothReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (isBluetoothOn(context)) {
            sendNotify(context, "Bluetooth is ON!");
            sendMyBroadcast(BLUETOOTH,STATUSON, context);
        } else {
            sendNotify(context, "Bluetooth is OFF!");
            sendMyBroadcast(BLUETOOTH,STATUSOFF, context);
        }
    }


    public static boolean isBluetoothOn(Context context) {

       BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter.isEnabled()) return true;
        else return false;
    }

    void sendNotify(Context context, String message) {

        Intent notificationIntent = new Intent(context, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                REQ_CODE, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Bluetooth mode")
                .setContentText(message)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));


        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);
    }
}
