package net7.xyz.android_home_13_receiver_notification.receivers;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v4.app.NotificationCompat;

import net7.xyz.android_home_13_receiver_notification.MainActivity;
import net7.xyz.android_home_13_receiver_notification.R;

import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSOFF;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSON;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.WIFI;
import static net7.xyz.android_home_13_receiver_notification.utils.NotifyUtils.sendMyBroadcast;


public class WifiReceiver extends BroadcastReceiver {

    public final int REQ_CODE = 103;
    public final int NOTIFY_ID = 104;

    public WifiReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (isWifiModeOn(context)) {
            sendNotify(context, "WIFI is ON!");
            sendMyBroadcast(WIFI,STATUSON, context);
        } else {
            sendNotify(context, "WIFI is OFF!");
            sendMyBroadcast(WIFI,STATUSOFF, context);
        }
    }

    public static boolean isWifiModeOn(Context context) {

        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled()) {
            return true;
        } else return false;
    }


    void sendNotify(Context context, String message) {

        Intent notificationIntent = new Intent(context, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                REQ_CODE, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("WIFI mode")
                .setContentText(message)
                .setVibrate(new long[]{100, 500, 200, 300, 300});

        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);
    }


}