package net7.xyz.android_home_13_receiver_notification.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import net7.xyz.android_home_13_receiver_notification.MainActivity;
import net7.xyz.android_home_13_receiver_notification.R;

import static net7.xyz.android_home_13_receiver_notification.MainActivity.NETWORK;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSOFF;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSON;
import static net7.xyz.android_home_13_receiver_notification.utils.NotifyUtils.sendMyBroadcast;

/**
 * Created by user on 18.02.2018.
 */

public class NetworkReceiver extends BroadcastReceiver {

    public final int REQ_CODE = 105;
    public final int NOTIFY_ID = 106;


    @Override
    public void onReceive(Context context, Intent intent) {

        if (isNetworkOn(context)) {
            sendNotify(context, "Network is ON!");
            sendMyBroadcast(NETWORK,STATUSON, context);
        } else {
            sendNotify(context, "Network is OFF!");
            sendMyBroadcast(NETWORK,STATUSOFF, context);
        }
    }

    public static boolean isNetworkOn(Context context) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {

            final ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (mobile != null) {
                //if (mobile.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
            }
            //}
            return false;

        } else {

            // version >= 7.0

            final ConnectivityManager connMgr = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            final android.net.NetworkInfo mobile = connMgr.getActiveNetworkInfo();
            //if (mobile != null&& mobile.isConnected()) {
            if (mobile != null) {
                //if (mobile.getType() == ConnectivityManager.TYPE_MOBILE) {
                return true;
                //}
            }
            return false;
        }
    }


    void sendNotify(Context context, String message) {

        Intent notificationIntent = new Intent(context, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                REQ_CODE, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Network")
                .setContentText(message)
                .setSmallIcon(R.drawable.networks);


        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);
    }


}