package net7.xyz.android_home_13_receiver_notification.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;

import net7.xyz.android_home_13_receiver_notification.MainActivity;
import net7.xyz.android_home_13_receiver_notification.R;

import static android.provider.Settings.Global.AIRPLANE_MODE_ON;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.AIRPLANE;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSOFF;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSON;
import static net7.xyz.android_home_13_receiver_notification.utils.NotifyUtils.sendMyBroadcast;


public class AirplaneReceiver extends BroadcastReceiver {

    public final int REQ_CODE = 101;
    public final int NOTIFY_ID = 102;

    public AirplaneReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (isAirplaneModeOn(context)) {
            sendNotify(context, "Airplane is ON!");
            sendMyBroadcast(AIRPLANE,STATUSON, context);
        } else {
            sendNotify(context, "Airplane is OFF!");
            sendMyBroadcast(AIRPLANE,STATUSOFF, context);
        }
    }

    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    AIRPLANE_MODE_ON, 0) != 0;
        }
    }


    void sendNotify(Context context, String message) {

        Intent notificationIntent = new Intent(context, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                REQ_CODE, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Airplane mode")
                .setContentText(message)
                .setTicker("Airplane mode!")
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM));
        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);


    }

}
