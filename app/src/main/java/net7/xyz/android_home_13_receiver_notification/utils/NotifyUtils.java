package net7.xyz.android_home_13_receiver_notification.utils;

import android.content.Context;
import android.content.Intent;


import static net7.xyz.android_home_13_receiver_notification.MainActivity.BROADCAST_ACTION;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUS;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSOFF;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.STATUSON;
import static net7.xyz.android_home_13_receiver_notification.MainActivity.TYPECONNECTION;

/**
 * Created by user on 21.02.2018.
 */

public class NotifyUtils {

    public static void sendMyBroadcast( String type,String status, Context context) {

        Intent myIntent = new Intent(BROADCAST_ACTION);
        if (status.equals(STATUSON)) {
            myIntent.putExtra(STATUS, STATUSON);
        } else {
            myIntent.putExtra(STATUS, STATUSOFF);
        }
        myIntent.putExtra(TYPECONNECTION, type);
        myIntent.setAction(BROADCAST_ACTION);
        context.sendBroadcast(myIntent);
    }
}
