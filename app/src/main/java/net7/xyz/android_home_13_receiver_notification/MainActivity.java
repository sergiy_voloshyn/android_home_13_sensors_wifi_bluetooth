package net7.xyz.android_home_13_receiver_notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Switch;

import butterknife.BindView;
import butterknife.ButterKnife;
import net7.xyz.android_home_13_receiver_notification.receivers.AirplaneReceiver;
import net7.xyz.android_home_13_receiver_notification.receivers.BluetoothReceiver;
import net7.xyz.android_home_13_receiver_notification.receivers.NetworkReceiver;
import net7.xyz.android_home_13_receiver_notification.receivers.WifiReceiver;

/*
1. Создать приложение, реагирующее на перевод смартфона в airplane mode. Когда на телефоне отключен
режим airplane mode - в status bar'e должен висеть notification с текстом "Большой брат следит за тобой".
 [удалить notification можно методом notificationManager.cancel(you_notify_id)]
1.1*. Улучшить приложение, добавив на экран activity статистику включенных передатчиков данных.
 Например: wifi - on; bluetooth - on, gsm - on и пр. Менять текст в notification в зависимости от
 включенных передатчиков (например, если включен только gsm - "Большой брат подглядывает за тобой",
  если включен еще один модуль - "Большой брат присматривает за тобой" и пр.)
1.2** Добавить для разных notification'ов разные отличительные характеристики на ваше усмотрение
(например это может быть вибрация, звуки, иконки и пр.). Каждый notification должен быть уникален.

 */
public class MainActivity extends AppCompatActivity {

    BroadcastReceiver broadcastReceiver;
    public static final String TYPECONNECTION = "TYPECONNECTION";
    public static final String STATUS = "STATUS";
    public static final String STATUSON = "STATUSON";
    public static final String STATUSOFF = "STATUSOFF";


    public static final String AIRPLANE = "AIRPLANE";
    public static final String BLUETOOTH = "BLUETOOTH";
    public static final String NETWORK = "NETWORK";
    public static final String WIFI = "WIFI";

    public final static String BROADCAST_ACTION = "net7.xyz.android_home_13_receiver_notification.mybroadcast";

    @BindView(R.id.switch_airplane_mode)
    Switch airplaneSwitch;

    @BindView(R.id.switch_bluetooth_mode)
    Switch bluetoothSwitch;

    @BindView(R.id.switch_network_mode)
    Switch networkSwitch;

    @BindView(R.id.switch_wifi_mode)
    Switch wifiSwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
//for Network >=7.0
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(new NetworkReceiver(), intentFilter);

//my receiver
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String typeConnection = intent.getStringExtra(TYPECONNECTION);
                String status = intent.getStringExtra(STATUS);

                if (status.equals(STATUSON)) {
                    switch (typeConnection) {
                        case AIRPLANE:
                            airplaneSwitch.setChecked(true);
                            break;
                        case BLUETOOTH:
                            bluetoothSwitch.setChecked(true);
                            break;
                        case NETWORK:
                            networkSwitch.setChecked(true);
                            break;
                        case WIFI:
                            wifiSwitch.setChecked(true);
                            break;
                    }
                }

                if (status.equals(STATUSOFF)) {
                    switch (typeConnection) {
                        case AIRPLANE:
                            airplaneSwitch.setChecked(false);
                            break;
                        case BLUETOOTH:
                            bluetoothSwitch.setChecked(false);
                            break;
                        case NETWORK:
                            networkSwitch.setChecked(false);
                            break;
                        case WIFI:
                            wifiSwitch.setChecked(false);
                            break;
                    }
                }
            }
        };


        IntentFilter myIntentFilter = new IntentFilter(BROADCAST_ACTION);
        registerReceiver(broadcastReceiver, myIntentFilter);

//check states at start
        if (AirplaneReceiver.isAirplaneModeOn(this)) {
            airplaneSwitch.setChecked(true);
        } else {
            airplaneSwitch.setChecked(false);
        }

        if (WifiReceiver.isWifiModeOn(this)) {
            wifiSwitch.setChecked(true);
        } else {
            wifiSwitch.setChecked(false);
        }
        if (NetworkReceiver.isNetworkOn(this)) {
            networkSwitch.setChecked(true);
        } else {
            networkSwitch.setChecked(false);
        }

        if (BluetoothReceiver.isBluetoothOn(this)) {
            bluetoothSwitch.setChecked(true);
        } else {
            bluetoothSwitch.setChecked(false);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

}
